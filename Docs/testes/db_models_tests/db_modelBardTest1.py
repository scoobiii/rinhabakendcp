import threading
import time

def run_test(num_threads, requests_per_thread):
  pool = ConnectionPool()

  start_time = time.time()
  threads = []
  for _ in range(num_threads):
    thread = threading.Thread(target=_run_requests, args=(pool, requests_per_thread))
    thread.start()
    threads.append(thread)

  for thread in threads:
    thread.join()

  elapsed_time = time.time() - start_time
  print(f"Completou {num_threads * requests_per_thread} requisições em {elapsed_time:.2f} segundos")

def _run_requests(pool, num_requests):
  for _ in range(num_requests):
    conn = pool.acquire()
    try:
      # Insira sua lógica de transação aqui
      cliente_id = 1
      valor = 100
      tipo = 'C'
      descricao = 'Depósito'
      create_transacao(conn, cliente_id, valor, tipo, descricao)
    finally:
      pool.release(conn)

if __name__ == "__main__":
  # Execute o teste com diferentes configurações de threads e requisições
  run_test(4, 1000000)
  run_test(8, 500000)
  run_test(16, 250000)
  run_test(32, 125000)
