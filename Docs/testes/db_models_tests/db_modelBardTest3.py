import sqlite3

# Conectar ao banco de dados SQLite em memória
DB_PATH = ":memory:"
conn = sqlite3.connect(DB_PATH)

# Criar tabela de exemplo
conn.execute("""
CREATE TABLE IF NOT EXISTS "clientes" (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT,
  "nome" TEXT NOT NULL,
  "saldo" INTEGER DEFAULT 0
);
""")

# Função para realizar transações com controle de versão
def transacao(cliente_id, valor, tipo, descricao):
  # Iniciar transação
  cursor = conn.cursor()
  cursor.execute("BEGIN TRANSACTION")

  # Obter versão da transação
  versao = cursor.execute("SELECT MAX(versao) FROM transacoes").fetchone()[0] or 0
  versao += 1

  # Registrar transação
  cursor.execute("""
    INSERT INTO transacoes (cliente_id, valor, tipo, descricao, versao)
    VALUES (?, ?, ?, ?, ?)
  """, (cliente_id, valor, tipo, descricao, versao))

  # Atualizar saldo
  if tipo == "C":
    cursor.execute("""
      UPDATE clientes SET saldo = saldo + ? WHERE id = ?
    """, (valor, cliente_id))
  elif tipo == "D":
    cursor.execute("""
      UPDATE clientes SET saldo = saldo - ? WHERE id = ? AND saldo >= ?
    """, (valor, cliente_id, valor))

  # Finalizar transação
  cursor.execute("COMMIT")

# Exemplo de uso
transacao(1, 500, "C", "Depósito")
transacao(1, -200, "D", "Compra")

# Obter saldo na versão atual
saldo = cursor.execute("""
  SELECT saldo FROM clientes AS c
  INNER JOIN transacoes AS t ON c.id = t.cliente_id
  WHERE t.versao = (SELECT MAX(versao) FROM transacoes)
  AND c.id = 1
""").fetchone()[0]

print(f"Saldo: {saldo}")

conn.close()
