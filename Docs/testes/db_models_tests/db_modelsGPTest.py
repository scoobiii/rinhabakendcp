import sqlite3
import threading
import time

# Define connection pool parameters
POOL_SIZE = 10

class ConnectionPool:
    def __init__(self):
        self.lock = threading.Lock()
        self.pool = [sqlite3.connect(":memory:") for _ in range(POOL_SIZE)]

    def acquire(self):
        with self.lock:
            if not self.pool:
                raise Exception("Connection pool exhausted")
            return self.pool.pop()

    def release(self, conn):
        with self.lock:
            self.pool.append(conn)

# Optimized map_to_row function (not used in this version)
def map_to_row(conn, id):
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM clientes WHERE id=?", (id,))
    return cursor.fetchone()

# Transaction function with error handling
def transaction(conn, cliente_id, valor, tipo, descricao):
    cursor = conn.cursor()
    try:
        cursor.execute("BEGIN TRANSACTION")
        # Update/insert logic
        cursor.execute("UPDATE saldos SET valor = valor + ? WHERE cliente_id = ?", (valor, cliente_id))
        cursor.execute("INSERT INTO transacoes (cliente_id, valor, tipo, descricao) VALUES (?, ?, ?, ?)",
                     (cliente_id, valor, tipo, descricao))
        cursor.execute("COMMIT")
    except Exception as e:
        cursor.execute("ROLLBACK")
        raise e
    finally:
        conn.close()  # Ensure connection is closed even if an exception occurs

# Load testing function (use with caution)
def load_test(num_threads, requests_per_thread):
    pool = ConnectionPool()

    start_time = time.time()
    threads = []
    for _ in range(num_threads):
        thread = threading.Thread(target=_run_requests, args=(pool, requests_per_thread))
        thread.start()
        threads.append(thread)

    for thread in threads:
        thread.join()

    elapsed_time = time.time() - start_time
    print(f"Completed {num_threads * requests_per_thread} requests in {elapsed_time:.2f} seconds")

def _run_requests(pool, num_requests):
    for _ in range(num_requests):
        conn = pool.acquire()
        try:
            # Replace with your actual transaction logic here
            cliente_id = 123  # Example data (replace with actual values)
            valor = -100
            tipo = 'D'
            descricao = 'Deposit'
            transaction(conn, cliente_id, valor, tipo, descricao)
        except Exception as e:
            print(f"Error in thread: {e}")
        finally:
            pool.release(conn)

# Example usage (replace with your actual transaction logic)
if __name__ == "__main__":
    # Run the load test with 4 threads and 100 requests per thread
    load_test(4, 100)
