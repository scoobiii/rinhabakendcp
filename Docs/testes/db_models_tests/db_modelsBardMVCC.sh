#!/bin/bash

# Conectar ao banco de dados SQLite em memória
DB_PATH=":memory:"

# Ativar o modo WAL (Write-Ahead Logging) para melhor desempenho e compatibilidade com MVCC
PRAGMA journal_mode = WAL;

# Criar um banco de dados em memória compartilhada acessível por várias threads
ATTACH DATABASE 'file:memdb1?mode=memory&cache=shared' AS aux1;

# Criar tabelas com timestamps para controle de versão
CREATE TABLE IF NOT EXISTS "clientes" (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT,
  "nome" TEXT NOT NULL,
  "limite" INTEGER,
  "saldo" INTEGER DEFAULT 0,
  "created_at" DATETIME DEFAULT CURRENT_TIMESTAMP,
  "updated_at" DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "transacoes" (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT,
  "cliente_id" INTEGER REFERENCES "clientes"("id"),
  "valor" INTEGER,
  "tipo" TEXT CHECK(tipo IN ('C', 'D')),
  "descricao" TEXT,
  "realizada_em" DATETIME DEFAULT CURRENT_TIMESTAMP,
  "created_at" DATETIME DEFAULT CURRENT_TIMESTAMP,
  "updated_at" DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "saldos" (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT,
  "cliente_id" INTEGER REFERENCES "clientes"("id"),
  "valor" INTEGER,
  "created_at" DATETIME DEFAULT CURRENT_TIMESTAMP,
  "updated_at" DATETIME DEFAULT CURRENT_TIMESTAMP
);

# Criar índices para otimizar o desempenho das consultas
CREATE INDEX IF NOT EXISTS idx_cliente_id ON transacoes(cliente_id);
CREATE INDEX IF NOT EXISTS idx_created_at ON transacoes(created_at);

# Definir funções auxiliares para interagir com as tabelas
function create_cliente() {
  local nome="$1"
  local limite="$2"
  sqlite3 "$DB_PATH" <<EOF
INSERT INTO "clientes" (nome, limite) VALUES ("$nome", $limite);
EOF
}

function create_transacao() {
  local cliente_id="$1"
  local valor="$2"
  local tipo="$3"
  local descricao="$4"
  sqlite3 "$DB_PATH" <<EOF
BEGIN TRANSACTION;
  UPDATE saldos SET valor = valor + ? WHERE cliente_id = ?;
  INSERT INTO transacoes (cliente_id, valor, tipo, descricao) VALUES (?, ?, ?, ?);
COMMIT;
EOF
}

function get_cliente() {
  local id="$1"
  sqlite3 "$DB_PATH" <<EOF
SELECT * FROM "clientes" WHERE id = $id;
EOF
}

function get_transacoes() {
  local cliente_id="$1"
  sqlite3 "$DB_PATH" <<EOF
SELECT * FROM "transacoes" WHERE cliente_id = $cliente_id ORDER BY realizada_em DESC;
EOF
}

function get_saldo() {
  local cliente_id="$1"
  sqlite3 "$DB_PATH" <<EOF
SELECT SUM(valor) AS saldo FROM transacoes WHERE cliente_id = $cliente_id;
EOF
}

# Exemplo de uso
create_cliente "João Silva" 1000
create_transacao 1 500 "C" "Depósito"
create_transacao 1 -200 "D" "Compra"

cliente=$(get_cliente 1) # Acesso otimizado usando o índice
echo "Cliente: $cliente"

transacoes=$(get_transacoes 1) # Acesso otimizado usando o índice e ordenação por data
echo "Transações:"
echo "$transacoes"

saldo=$(get_saldo 1) # Cálculo do saldo atual
echo "Saldo: $saldo"
