import threading
import time
from db_modelsBard import ConnectionPool, create_transacao

# Definir parâmetros do teste
NUM_THREADS = 4
NUM_TRANSACTIONS_PER_THREAD = 1000000

# Criar pool de conexões
pool = ConnectionPool()

def run_thread(pool):
  for _ in range(NUM_TRANSACTIONS_PER_THREAD):
    conn = pool.acquire()
    create_transacao(1, 100, "C", "Depósito", conn)
    pool.release(conn)

start_time = time.time()

# Iniciar threads
threads = [threading.Thread(target=run_thread, args=(pool,)) for _ in range(NUM_THREADS)]
for thread in threads:
  thread.start()

# Aguardar o término das threads
for thread in threads:
  thread.join()

elapsed_time = time.time() - start_time

print(f"Tempo total: {elapsed_time:.2f} segundos")
print(f"Transações por segundo: {NUM_THREADS * NUM_TRANSACTIONS_PER_THREAD / elapsed_time:.2f}")
