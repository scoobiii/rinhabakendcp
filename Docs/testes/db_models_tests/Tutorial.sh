 **Tutorial 1: Opening a Database in Memory on GCP and Applying a Python Script**

**Prerequisites:**

- A GCP account with a project and a terminal window (Cloud Shell or SSH)
- Python 3.x installed on the GCP instance
- SQLite3 library for Python (usually included by default)

**Steps:**

1. **Opening the Database in Memory:**

   - **Access the GCP terminal:** Navigate to the Cloud Shell or connect using SSH.
   - **Create the database schema:** Run the following command in the terminal:

     ```bash
     sh db_modelsGPT.sh
     ```

     This will create the database schema in an in-memory SQLite database named `db.sqlite`.

2. **Applying the Python Script:**

   - **Run the Python script:** Execute the following command in the terminal:

     ```bash
     python db_modelsGPTBardTest.py
     ```

     This will perform the following actions:

     - Establish a connection pool for managing database connections efficiently.
     - Define functions for transactions and load testing.
     - Conduct a load test with multiple threads and simulated transactions.
     - Print the results of the load test, indicating the time taken to complete the requests.

**Key Points:**

- The database is created in memory, meaning it will not persist after the terminal session is closed.
- The `map_to_row` function in the Python script is not used in this version.
- The `load_test` function is designed for testing purposes and should be used with caution in production environments.
- Replace the placeholder transaction logic in the `_run_requests` function with your specific database operations.

**Additional Information:**

- For more details on SQLite: [https://www.sqlite.org/index.html](https://www.sqlite.org/index.html)
- For more information on Python's SQLite3 library: [https://docs.python.org/3/library/sqlite3.html](https://docs.python.org/3/library/sqlite3.html)
- For more guidance on GCP: [https://cloud.google.com/docs](https://cloud.google.com/docs)

**Documentation:**

- It's recommended to create documentation that covers:
    - Purpose of the database and script
    - Detailed explanation of each function and its role
    - Instructions for modifying the transaction logic for specific use cases
    - Potential errors and troubleshooting steps
    - Examples of how to use the script for different scenarios

 **Tutorial 2: 

 ## Tutorial: Criando e Aplicando um Banco de Dados In-Memory no GCP com Python

Este tutorial demonstra como criar um banco de dados in-memory no Google Cloud Platform (GCP) usando Python e aplicá-lo em um script de teste de carga.

**Pré-requisitos:**

* Uma conta do Google Cloud Platform.
* Python 3 instalado em sua máquina local.
* Conhecimento básico de terminais Linux e comandos do banco de dados SQLite.

**Etapas:**

**1. Criando o Script de Banco de Dados (db_modelsGPT.sh):**

1. Abra um editor de texto e salve o script com o nome `db_modelsGPT.sh`.
2. Copie o código a seguir no script:

```bash
#!/bin/bash

sqlite3 db.sqlite <<EOF

CREATE TABLE clientes (
  id INTEGER PRIMARY KEY,
  nome VARCHAR(50) NOT NULL,
  limite INTEGER,
  saldo INTEGER DEFAULT 0
);

CREATE TABLE transacoes (
  id INTEGER PRIMARY KEY,
  cliente_id INTEGER,
  valor INTEGER,
  tipo VARCHAR(1),
  descricao VARCHAR(10),
  realizada_em DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE saldos (
  id INTEGER PRIMARY KEY,
  cliente_id INTEGER,
  valor INTEGER
);

EOF

Map objects to rows
function map_to_row() {
  resultado=$(sqlite3 db.sqlite "SELECT * FROM clientes WHERE id=${id}")

  Parse resultado and return object
  cliente=($resultado)

  echo ${cliente[@]}
}

Open database
function open_db() {
  sqlite3 ":memory:" db.sqlite
}

```

3. Este script define o esquema do banco de dados com três tabelas: `clientes`, `transacoes` e `saldos`. Ele também inclui duas funções utilitárias: `map_to_row` para recuperar dados de um cliente específico e `open_db` para abrir uma conexão com o banco de dados in-memory.

**2. Criando o Script de Teste de Carga (db_modelsGPTBardTest.py):**

1. Abra um novo editor de texto e salve o script com o nome `db_modelsGPTBardTest.py`.
2. Copie o código a seguir no script:

```python
import sqlite3
import threading
import time

# Define connection pool parameters
POOL_SIZE = 10

class ConnectionPool:
  def __init__(self):
    self.lock = threading.Lock()
    self.pool = [sqlite3.connect(":memory:") for _ in range(POOL_SIZE)]

  def acquire(self):
    with self.lock:
      if not self.pool:
        raise Exception("Connection pool exhausted")
      return self.pool.pop()

  def release(self, conn):
    with self.lock:
      self.pool.append(conn)

# Transaction function with error handling
def transaction(conn, cliente_id, valor, tipo, descricao):
  cursor = conn.cursor()
  try:
    cursor.execute("BEGIN TRANSACTION")
    # Update/insert logic
    cursor.execute("UPDATE saldos SET valor = valor + ? WHERE cliente_id = ?", (valor, cliente_id))
    cursor.execute("INSERT INTO transacoes (cliente_id, valor, tipo, descricao) VALUES (?, ?, ?, ?)",
                  (cliente_id, valor, tipo, descricao))
    cursor.execute("COMMIT")
  except Exception as e:
    cursor.execute("ROLLBACK")
    raise e
  finally:
    conn.close()  # Ensure connection is closed even if an exception occurs

# Load testing function (use with caution)
def load_test(num_threads, requests_per_thread):
  pool = ConnectionPool()

  start_time = time.time()
  threads = []
  for _ in range(num_threads):
    thread = threading.Thread(target=_run_requests, args=(pool, requests_per_thread))
    thread.start()
    threads.append(thread)

  for thread in threads:
    thread.join()

  elapsed_time = time.time() - start_time
  print(f"Completed {num_threads * requests_per_thread} requests in {elapsed_time:.2f} seconds")

def _run_requests(pool, num_requests):
  for _ in range(num_requests):
    conn = pool.acquire()
    try:
      # Replace with your actual transaction logic here
      # Example:


 **Tutorial 3: 

 ## Tutorial: Criando e Aplicando um Banco de Dados In-Memory no GCP com Python

Este tutorial demonstra como criar um banco de dados in-memory no Google Cloud Platform (GCP) usando Python e aplicá-lo em um script de teste de carga.

**Pré-requisitos:**

* Uma conta do Google Cloud Platform.
* Python 3 instalado em sua máquina local.
* Conhecimento básico de terminais Shell (como Bash).
* Familiaridade com SQLite e Python.

**Etapas:**

**1. Criando o Script de Banco de Dados (db_modelsGPT.sh):**

1. Abra um editor de texto e salve o código abaixo como `db_modelsGPT.sh`.
2. Este script cria o banco de dados SQLite in-memory chamado `db.sqlite`.
3. Define funções para mapear objetos para linhas e abrir a conexão do banco de dados.

```bash
#!/bin/bash

sqlite3 db.sqlite <<EOF

CREATE TABLE clientes (
id INTEGER PRIMARY KEY,
nome VARCHAR(50) NOT NULL,
limite INTEGER,
saldo INTEGER DEFAULT 0
);

CREATE TABLE transacoes (
id INTEGER PRIMARY KEY,
cliente_id INTEGER,
valor INTEGER,
tipo VARCHAR(1),
descricao VARCHAR(10),
realizada_em DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE saldos (
id INTEGER PRIMARY KEY,
cliente_id INTEGER,
valor INTEGER
);

EOF

function map_to_row() {
resultado=$(sqlite3 db.sqlite "SELECT * FROM clientes WHERE id=${id}")

# Parse resultado and return object
cliente=($resultado)

echo ${cliente[@]}
}

function open_db() {
sqlite3 ":memory:" db.sqlite
}

```

**2. Criando o Script de Teste de Carga (db_modelsGPTBardTest.py):**

1. Abra um editor de texto e salve o código abaixo como `db_modelsGPTBardTest.py`.
2. Este script implementa um pool de conexões para gerenciar várias solicitações simultâneas.
3. Define uma função `transaction` para realizar transações com tratamento de erros.
4. Inclui uma função `load_test` para simular carga no banco de dados.

```python
import sqlite3
import threading
import time

# Define connection pool parameters
POOL_SIZE = 10

class ConnectionPool:
    def __init__(self):
        self.lock = threading.Lock()
        self.pool = [sqlite3.connect(":memory:") for _ in range(POOL_SIZE)]

    def acquire(self):
        with self.lock:
            if not self.pool:
                raise Exception("Connection pool exhausted")
            return self.pool.pop()

    def release(self, conn):
        with self.lock:
            self.pool.append(conn)

# Optimized map_to_row function (not used in this version)
def map_to_row(conn, id):
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM clientes WHERE id=?", (id,))
    return cursor.fetchone()

# Transaction function with error handling
def transaction(conn, cliente_id, valor, tipo, descricao):
    cursor = conn.cursor()
    try:
        cursor.execute("BEGIN TRANSACTION")
        # Update/insert logic
        cursor.execute("UPDATE saldos SET valor = valor + ? WHERE cliente_id = ?", (valor, cliente_id))
        cursor.execute("INSERT INTO transacoes (cliente_id, valor, tipo, descricao) VALUES (?, ?, ?, ?)",
                      (cliente_id, valor, tipo, descricao))
        cursor.execute("COMMIT")
    except Exception as e:
        cursor.execute("ROLLBACK")
        raise e
    finally:
        conn.close()  # Ensure connection is closed even if an exception occurs

# Load testing function (use with caution)
def load_test(num_threads, requests_per_thread):
    pool = ConnectionPool()

    start_time = time.time()
    threads = []
    for _ in range(num_threads):
        thread = threading.Thread(target=_run_requests, args=(pool, requests_per_thread))
        thread.start()
        threads.append(thread)

    for thread in threads:
        thread.join()

    elapsed_time = time.time() - start_time
    print(f"Completed {num_threads * requests_per_thread} requests in {elapsed_time:.2f} seconds")

def _run_requests(pool, num_