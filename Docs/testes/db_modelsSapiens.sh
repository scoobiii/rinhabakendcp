
#!/bin/bash

# Variáveis de ambiente
export GOOGLE_APPLICATION_CREDENTIALS=/caminho/para/credenciais.json
export PROJECT_ID=quantum-412423
export REGION=us-central1

# Conectar ao banco de dados SQLite em memória
sqlite3 :memory:

# Criar tabela
CREATE TABLE usuarios (id INTEGER PRIMARY KEY, nome TEXT, email TEXT);

# Inserir dados
INSERT INTO usuarios (nome, email) VALUES ('Zé Sobrinho', 'zeh.sobrinho@gmail.com');

# Selecionar dados
SELECT * FROM usuarios;

# Desconectar do banco de dados
.exit
