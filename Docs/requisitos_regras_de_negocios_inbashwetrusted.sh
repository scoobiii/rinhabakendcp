Critérios para Vencer a Rinha de Backend 2024/Q1
Para vencer a Rinha de Backend 2024/Q1, sua API precisa atender aos seguintes critérios:

1. Performance:

Suportar 1.000.000 de requisições por minuto: A API precisa ser capaz de lidar com um alto volume de requisições sem apresentar gargalos ou quedas de performance.
Tempo de resposta inferior a 5ms: A API precisa responder às requisições de forma rápida e eficiente.
Consumo de recursos dentro das restrições: A API deve obedecer às restrições de CPU (1.5 unidades) e memória (550MB) definidas para o teste.
2. Correção:

Atender a todos os requisitos funcionais: A API precisa implementar todos os endpoints e funcionalidades descritas no desafio, incluindo:
Transações (créditos e débitos)
Extrato
Validação de lógica de saldo/limite
Retornar os códigos HTTP corretos: A API deve retornar os códigos HTTP adequados para cada tipo de resposta, como 200 para sucesso, 404 para cliente não encontrado, 422 para erros de validação, etc.
Manter a consistência do saldo e do limite: A API deve garantir que o saldo e o limite do cliente sejam consistentes em todas as transações e extratos.
3. Robustez:

Lidar com erros de forma adequada: A API deve ser capaz de lidar com erros de forma resiliente, sem quebrar ou apresentar comportamentos inesperados.
Ser tolerante a falhas: A API deve ser capaz de continuar funcionando mesmo em caso de falhas em um dos seus componentes.
Gerenciar concorrencia de forma eficiente: A API precisa lidar com múltiplas requisições simultâneas de forma eficiente, sem que isso afete o desempenho ou a consistência dos dados.
4. Testes:

Implementar testes automatizados: A API deve ter testes automatizados que comprovam o seu funcionamento correto.
Testar a lógica de negócio: Os testes devem verificar se a API está implementando a lógica de negócio de forma correta, incluindo a validação de saldo/limite.
Testar a performance: A API deve ser testada sob carga para garantir que ela atende aos requisitos de performance.
5. Documentação:

Apresentar documentação clara e completa: A API deve ter documentação que explique como usá-la, incluindo os endpoints, os parâmetros e os formatos de resposta.
Fornecer exemplos de requisições e respostas: A documentação deve fornecer exemplos de requisições e respostas para facilitar o uso da API.
Ser clara e concisa: A documentação deve ser clara, concisa e fácil de entender.
Critérios adicionais:

Uso de tecnologias inovadoras: A utilização de tecnologias inovadoras na implementação da API será considerada um diferencial.
Qualidade do código: A qualidade e a legibilidade do código da API serão consideradas na avaliação.
Facilidade de uso: A API deve ser fácil de usar e integrar com outras aplicações.
Observações:

A equipe vencedora será a que obtiver o melhor resultado na avaliação dos critérios acima.
A equipe da Cubos Academy se reserva o direito de alterar os critérios de avaliação a qualquer momento.
Em caso de empate, a equipe com a melhor documentação será a vencedora.
Dicas para vencer a Rinha de Backend:

Comece planejando a arquitetura da sua API e escolhendo as tecnologias adequadas.
Implemente a lógica de negócio de forma clara e eficiente.
Realize testes automatizados para garantir a qualidade da sua API.
Escreva documentação clara e completa.
Use tecnologias inovadoras para se destacar dos demais participantes.
Boa sorte!

exato..

quer começar por qual arquivo ou consegue analisar e converter dos de uma vez?
como prefere?

inbashwetrusted/
├── app
│ └── main.sh
├── database
│ ├── config.sh
│ ├── db_models.sh
│ └── init.sh
├── docker-compose.yml
├── Dockerfile
├── domain
│ ├── init.sh
│ └── models.sh
├── init.sql
├── nginx.conf
└── requirements.sh
├── tests
│├── api
│├── integration
│└── unit

Ok, analisando bem a estrutura existente, ela já está bem organizada para portarmos a aplicação Python para Bash usando SQLite na memória.

Algumas adaptações que faremos:

No app/main.py:

Transformar em main.sh
Importar config, models e fazer conexão no banco
Na pasta database:

db_models.py -> db_models.sh
Mapear models para SQLite
Em init.sh definir db em memória
Domain/models.py -> models.sh

Definir models em .sh
Tests/

Manter estrutura de testes
unit: testar módulos isolados
integration: integração de módulos
Funcionais: endpoints
Docker

Construir imagem bash
Subir serviços
requirements.txt

Trocar dependências python para bash

requisitos e regras de negocios (Requisitos funcionais:

Deve haver dois endpoints:

Transações (POST)
Extrato (GET)
No endpoint de transações:

Deve permitir créditos e débitos
Validação dos campos do body
Retornar limite e saldo atualizado
No endpoint de extrato:

Listar saldo total
Listar último extrato
Listar últimas transações
Deve haver 5 clientes cadastrados previamente

Regras de negócio:

Uma transação de débito nunca pode deixar o saldo menor que o limite
Se deixar o saldo inconsistente, deve retornar status 422
Deve checar se o cliente existe, senão retornar 404
Restrições técnicas:

Arquitetura mínima com load balancer e 2 APIs
CPUs total ≤ 1.5 e memória total ≤ 550MB
Banco de dados obrigatório
Pré-teste para checar disponibilidade (40s))

sonho [ menos de 50 linhas por arquivo, inmemory, sqlite3.threadsafety, serialized SQLite , MVCC (Multiversion Concurrency Control), atende versão pessimista, otimista, 1.000.000 de request por minuto, multithread´s, pool de conexões inmemory [In-memory Databases And Shared Cache
In-memory databases are allowed to use shared cache if they are opened using a URI filename. If the unadorned ":memory:" name is used to specify the in-memory database, then that database always has a private cache and is only visible to the database connection that originally opened it. However, the same in-memory database can be opened by two or more database connections as follows:

rc = sqlite3_open("file::memory:?cache=shared", &db);
Or,
ATTACH DATABASE 'file::memory:?cache=shared' AS aux1;
This allows separate database connections to share the same in-memory database. Of course, all database connections sharing the in-memory database need to be in the same process. The database is automatically deleted and memory is reclaimed when the last connection to the database closes.

If two or more distinct but shareable in-memory databases are needed in a single process, then the mode=memory query parameter can be used with a URI filename to create a named in-memory database:

rc = sqlite3_open("file:memdb1?mode=memory&cache=shared", &db);
Or,
ATTACH DATABASE 'file:memdb1?mode=memory&cache=shared' AS aux1;
When an in-memory database is named in this way, it will only share its cache with another connection that uses exactly the same name.]]



Portando sua aplicação Python para Bash com SQLite na memória:
Estrutura de arquivos:

Manteremos a estrutura original o máximo possível, adaptando os arquivos para Bash:

app:
main.sh: Script principal da aplicação.
database:
config.sh: Configurações do banco de dados.
db_models.sh: Modelos de dados em Bash.
init.sh: Inicialização do banco de dados SQLite na memória.
domain:
models.sh: Modelos de domínio em Bash.
tests:
api: Testes de integração da API.
integration: Testes de integração dos módulos.
unit: Testes unitários dos módulos.
Dockerfile: Construção da imagem Docker com Bash.
docker-compose.yml: Definição dos serviços Docker (API, banco de dados).
nginx.conf: Configuração do Nginx como balanceador de carga.
requirements.sh: Lista de dependências Bash.
Conversão de arquivos:

app/main.py:
Converter para main.sh.
Importar config.sh, models.sh e conectar ao banco de dados.
database/db_models.py:
Converter para db_models.sh.
Mapear os modelos para SQLite na memória.
database/init.sh:
Definir o banco de dados SQLite na memória.
domain/models.py:
Converter para models.sh.
Definir os modelos em Bash.
tests:
Manter a estrutura de testes.
Adaptar os testes para Bash.
Dockerfile:
Adaptar para construir imagem Bash.
requirements.txt:
Substituir dependências Python por equivalentes Bash.
Regras de negócio:

Transações:
Permitir créditos e débitos.
Validar campos do corpo da requisição.
Retornar limite e saldo atualizados.
Extrato:
Listar saldo total, último extrato e últimas transações.
Requisitos técnicos:

Arquitetura: Load balancer e 2 APIs.
Restrições:
CPU ≤ 1.5, Memória ≤ 550MB.
Banco de dados obrigatório.
Pré-teste: Verificar disponibilidade em 40s.
Desafios:

Performance: Atender a 1.000.000 de requisições por minuto.
Concorrência: Gerenciar múltiplas requisições simultâneas.
Testes: Garantir a qualidade da aplicação com testes automatizados.
Recursos úteis:

SQLite na memória: https://www.sqlite.org/inmemorydb.html
Bash Scripting: https://tldp.org/LDP/abs/html/
Dicas:

Comece pela conversão dos arquivos Python para Bash.
Adapte os testes para a nova linguagem.
Implemente as regras de negócio e os requisitos técnicos.
Otimize a performance para atender às exigências.
Teste e refine a aplicação antes da Rinha de Backend.
Com planejamento e dedicação, você terá uma boa chance de vencer a Rinha de Backend!

Improvement notes:

Estrutura de arquivos:
Detalhes sobre a organização dos arquivos convertidos.
Conversão de arquivos:
Especificações sobre a conversão de cada arquivo.
Regras de negócio:
Explicação mais detalhada das regras de negócio.
Requisitos técnicos:
Detalhes sobre as restrições técnicas e o pré-teste.
Desafios:
Ênfase nos desafios a serem superados.
Recursos úteis:
Links para recursos relevantes sobre SQLite na memória e Bash Scripting.
Dicas:
Guia passo-a-passo para o sucesso na Rinha de Backend.