
# abre este cara no shell em memoria e aplica 
# 1) teste unitario
# 2) funcional
# 3) integração

#!/bin/bash

# Define database path
DB_PATH=":memory:"

# Create tables using SQLite syntax
sqlite3 $DB_PATH <<EOF
CREATE TABLE "clientes" (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT,
  "nome" TEXT NOT NULL,
  "limite" INTEGER,
  "saldo" INTEGER DEFAULT 0
);

CREATE TABLE "transacoes" (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT,
  "cliente_id" INTEGER REFERENCES "clientes"("id"),
  "valor" INTEGER,
  "tipo" TEXT CHECK(tipo IN ('C', 'D')),
  "descricao" TEXT,
  "realizada_em" DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "saldos" (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT,
  "cliente_id" INTEGER REFERENCES "clientes"("id"),
  "valor" INTEGER
);
EOF

# Define helper functions for interacting with tables
function create_cliente() {
  local nome="$1"
  local limite="$2"
  sqlite3 $DB_PATH <<EOF
INSERT INTO "clientes" (nome, limite) VALUES ("$nome", $limite);
EOF
}

function create_transacao() {
  local cliente_id="$1"
  local valor="$2"
  local tipo="$3"
  local descricao="$4"
  sqlite3 $DB_PATH <<EOF
INSERT INTO "transacoes" (cliente_id, valor, tipo, descricao) VALUES ($cliente_id, $valor, "$tipo", "$descricao");
EOF
}

function get_cliente() {
  local id="$1"
  sqlite3 $DB_PATH <<EOF
SELECT * FROM "clientes" WHERE id = $id;
EOF
}

function get_transacoes() {
  local cliente_id="$1"
  sqlite3 $DB_PATH <<EOF
SELECT * FROM "transacoes" WHERE cliente_id = $cliente_id;
EOF
}

function get_saldo() {
  local cliente_id="$1"
  sqlite3 $DB_PATH <<EOF
SELECT SUM(valor) AS saldo FROM "transacoes" WHERE cliente_id = $cliente_id;
EOF
}

# Example usage
create_cliente "João Silva" 1000
create_transacao 1 500 "C" "Depósito"
create_transacao 1 -200 "D" "Compra"

cliente=$(get_cliente 1)
echo "Cliente: $cliente"

transacoes=$(get_transacoes 1)
echo "Transações:"
echo "$transacoes"

saldo=$(get_saldo 1)
echo "Saldo: $saldo"

